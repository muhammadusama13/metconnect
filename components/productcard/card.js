import Link from "next/link";
import React, { useState } from "react";
import Modal from "../modal/modal";

const Card = ({ className, name, img, discount, discountprice, describtion, price }) => {
  const [open, setOpen] = useState(false);
  return (
    <>
      <Modal hide={setOpen} show={open}>
        <section className="py-30px px-6">
          <button className="block w-6 ml-auto" onClick={() => setOpen(false)}>
            <img src="images/cross2.svg" />
          </button>
          <section className="sm:flex items-center">
            <div className="w-full sm:w-1/2">
              <img src={img} className="w-full h-456px object-contain" />
            </div>
            <div className="w-full sm:w-1/2 pl-0 sm:pl-30px">
              <h1 className="font25 font-bold text-darkblack ">{name}</h1>
              {/* <div className="flex mt-6">
                <img src="images/fstar.svg" className="mr-1" />
                <img src="images/fstar.svg" className="mr-1" />
                <img src="images/fstar.svg" className="mr-1" />
                <img src="images/estar.svg" className="mr-1" />
                <img src="images/estar.svg" className="mr-1" />
                <p className="text-darkwhite text-sm ">( 1 Customer Review )</p>
              </div> */}
              <div className="mt-2 flex">
                {discount && (
                  <p className="line-through text-darkblack text-sm font-semibold mr-2">
                    ${discountprice}
                  </p>
                )}
                <p className="text-orange text-sm font-semibold">${price}</p>
              </div>
              <p className="text-sm mt-6 text-darkblack">
                {describtion}
              </p>
              <div className="mt-6 flex">
                <div className="border w-77px h-50px flex items-center justify-around text-darkblack text-base">
                  <button>-</button>
                  <span>01</span>
                  <button>+</button>
                </div>
                <button className="bg-orange text-white text-lg font-semibold h-50px w-217px ml-4">
                  ADD TO CART
                </button>
                <button className="border w-50px h-50px flex items-center justify-center  ml-4">
                  <img src="images/heart.svg" />
                </button>
              </div>
              {/* <div className="flex mt-6">
                <p className="text-darkblack text-sm mr-10">Color:</p>
                <button className="border h-5 w-5  p-0.5  mx-1">
                  <span className="bg-black h-full w-full block "></span>
                </button>
                <button className="border h-5 w-5  p-0.5  mx-1">
                  <span className="bg-litegray h-full w-full block "></span>
                </button>
                <button className="border h-5 w-5  p-0.5  mx-1">
                  <span className="bg-gray-400 h-full w-full block "></span>
                </button>
              </div> */}
            </div>
          </section>
        </section>
      </Modal>
      <div className={`${className}  product_card_section`}>
        <div className="relative overflow-y-hidden ">
          <img src={img} className="w-full h-269px object-cover" />
          <div className="absolute top-0 flex w-full flex-wrap justify-center h-full product_card">
            <button className="bg-white w-7 h-7 flex items-center justify-center mx-1">
              <img src="images/heart.svg" />
            </button>
            <button
              className="bg-white w-7 h-7 flex items-center justify-center mx-1"
              onClick={() => setOpen(true)}
            >
              <img src="images/eye.svg" />
            </button>
            <button className="bg-black w-full text-white h-9 flex items-center justify-center">
              <img src="images/cart2.svg" className="mr-4" />
              Add to cart
            </button>
          </div>
        </div>
        <Link href="/productdetails">
          <a className="text-black text-sm font-semibold mt-2.5">{name}</a>
        </Link>
        <div className="mt-2 flex">
          {discount && (
            <p className="line-through text-darkblack text-sm font-semibold mr-2">
              ${discountprice}
            </p>
          )}
          <p className="text-orange text-sm font-semibold">${price}</p>
        </div>
      </div>
    </>
  );
};

export default Card;
