export const FeaturedProducts = [
    {
        name: "Alritz Fairy Lantern Craft Kit",
        category: 'Arts and Crafts',
        Description: `
        Everything includes inbox: This craft kit includes a pair of glass jars, fairy sticks, wish paper, butterflies, candle lights, remote controls, roses, ribbons, glitter, glue, white paper, twine, 3D dogs, houses, and everything you need to create your own decorative jar!
        Stimulate imagination: The accompanying items make handmade DIY infinite possibilities. When children make lanterns, their imagination is fully utilized. You will be amazed by the creativity of children, and each work is unique, which will be children’s good memory. And the child can even make it with parents and become a fun family activity that kids will love.
        Gifts for girls aged 4 5 6 7 8 9 10 11 12: Through this interesting handicraft activity, create a little fairy tale magic and miracle. This is a great substitute for toys and the colorful gift box comes with instructions. Recommended gifts for Christmas, Easter, Children’s Day and Halloween.
        Night light color changing with remote: Just need to add a battery and use it as a night light or summer terrace decoration. Girls can use the mason jar in the bedroom, set the sleep timer through the remote control, set the color that they want, and fall asleep with the fairy. The soft lights and the fairy bring sweet dreams. Note: Each candlelight needs 2 AAA batteries (batteries not included)
        Safety Material: parents please rest assured to buy. Our materials have undergone strict safety testing to ensure that your children are 100% safe and free of harmful chemicals.
        `,
        price: '32.00',
        discount: '35.00',
        images: '/images/images/ArtsandCrafts.jpg'
    },
    {
        name: "Art Set 85 Piece with Built-in Wooden Easel",
        category: 'Arts and Crafts',
        Description: `
        With this 85 pieces art set you can begin drawing and painting as soon as you open the box. This art set includes 12 sketching pencils(B-6B,HB,H-5H), 12 watercolor cakes, 8 acrylic paints, 28 watercolor pencils, 10 artist brushes, 8 oil pastels, 1 plastic palette, 1 putty eraser, 1 wooden sharpener，1 x 50 page sketch book, 1 x 24 page drawing pad, 1 built-in easel+1 wooden case（13.8”x 3.9” x 10.2”）.
        This art set is suitable for both students, hobbyists who love scrapbooks and even professional drawers working on craft or construction paper, detail sketching and high level artistic skills.
        Acrylic paint colors feature a density that is balanced while fluid and flexible. Essential acrylic paint brushes feature comfort handles and soft flexible nylon bristles.
        Comes in a wooden carrying case with a drawer to easily organize your supplies. Take your art set with you anywhere to color and draw.
`,
        price: '45.00',
        discount: '48.00',
        images: '/images/images/ArtsandCrafts2.jpg'
    },
    {
        name: "Art Set with 2 Drawing Pad (126 Piece)",
        category: 'Arts and Crafts',
        Description: `
        Beginner to Professionals: With this 126 Pieces Deluxe Art Creativity set you can begin drawing and painting as soon as you open the box. Comes complete with all the necessary tools for a beginning artist all the way to a veteran. Perfect art set for adults and kids.
        The Essentials: 24 acrylic paints,24 oil pastels,24 color pencils,24 watercolor cakes,3×8 well palettes,2 paint brushes,2 sketch pencils,1 wooden sharpener,1metal sharpener,1 sandpaper block,1 eraser,2 putty eraser,1plastic palette,9 artist brushes,4pcs 8 x 10 Canvas,1 x 50 Page Drawing Pad,1 x 24 Page Acrylic Pad,1 Wooden Case. This set has everything you need to be an artist.
        Widely Use: This art set is perfect for both students, hobbyists who love scrapbooks and even professional drawers working on craft or construction paper, detail sketching and high-level artistic skills, it is best tools to showcase someone’s awesome drawing talent.
        Safe & Environmental: This drawing and painting art set is no harm for children. All the art supplies are in one wooden case and each crayons, pencils and pastels in this art box are non-toxic and safe. It is easy to carry and safe to use.
        Tra vel Sized Kit: Our gorgeous colors and design tools are neatly organized and arranged in a wood case so you can create art wherever you go.
`,
        price: '42.99 ',
        discount: '45.00',
        images: '/images/images/ArtsandCrafts3.jpg'
    },
    {
        name: "ARTMIX Dinosaur Painting Kit",
        category: 'Arts and Crafts',
        Description: `
        The Kids Art Kit They Can Actually Play With – Heavy Duty ABS Plastic Dinosaurs and non-toxic, washable paint promote more than hand eye coordination and stronger color identification; this built-kid-tough dinosaur toy set delivers years of imaginative fun, without chipping, breaking or losing their kid designed charm. Optimized dinosaur toys for kids age 3-5 with plenty of detailed work for children up to 12 to join in the fun!
        Plenty for Little Creatives – Won’t Run Out of Paint: Stop those little tears before they begin! While most toddler arts and crafts barely include enough paint to cover the dinosaur toys for kids 5-7, we include a full extra paint set – 3 instead of 2 – so every dino lover can paint to their hearts desire without worrying about running out.
        Parent Approved Fun Kit – Everything You Need, No Silly Fillers: Don’t fall for those overpadded kids painting set that include countless useless garbage, but not what your kiddo really wants. ARTMIX activities for kids age 4-8 contains the premium quality dinosaur figurines that keeps them engaged for hours – 4 Large Dinosaurs, 6 Small Dinosaurs, 3 Fast Drying Paint Sets, 3 Kids Paint Brushes, 2 Paint Mixing Pallets & 2 sets of cool stickers.
        A Well-Deserved Break…or More Family Time: Does it ever feel like you run a million miles a week? Whether you’re looking for the ideal birthday present, a way to foster stronger family ties, or hours of distraction to keep the kids busy while you tackle projects or take a well deserved break, we guarantee this will do the trick whether you’re looking for dinosaur toys for kids 8-12 or a younger kids painting set.
        Certified Kid Safe – 100% Satisfaction & Money Back Guarantee: MSDS certified and meticulously inspected and tested to ensure it’s ultra-durable, long lasting, and toxin-free, this is the kids activity kit to get if you want to create cherished memories that they can play with as they live, laugh and grow. Click add to cart. Appreciate the joy it brings to your family. Enjoy the durability or your money back. We’re that confident you’ll love it!
        `,
        price: '15.00',
        discount: '18.00 ',
        images: '/images/images/ArtsandCrafts4.jpg'
    },
    {
        name: "Bracelet Making Kit",
        category: 'Arts and Crafts',
        Description: `
        Beads for Bracelets Making Pony Beads Polymer Clay Beads Smile 
        Face Beads Letter Beads for Jewelry Making, DIY Arts and Crafts Gifts for Girls Age 6 7 8 9 10-12`,
        price: '15.89',
        discount: null,
        images: '/images/images/ArtsandCrafts5.jpg'
    },
    {
        name: "Made By Me Create Your Own Sand Art – 4 Sand Bottles & 2 Pendent Bottles with 8 Bright Sand Colors",
        category: 'Arts and Crafts',
        Description: `
        Create cool designs: learn how to layer colorful patterns to create one-of-a-kind designs
Fun intro into different medias of art: express yourself and discover art through different art mediums
Display with flair: fill fun shaped sand bottles with playful decorations and to create personalized gifts
Kit includes: 4 sand bottles, 2 pendant bottles, 8 colors of sand, 1 satin cord, 1 plastic funnel, 1 design tool, easy-to-follow instructions`,
        price: '16.00',
        discount: '18.00',
        images: '/images/images/ArtsandCrafts6.jpg'
    },
    {
        name: "Sewing Kit for Traveler",
        category: 'Arts and Crafts',
        Description: `
        Adults, Beginner, Emergency, DIY Sewing Supplies Organizer
         Filled with Scissors, Thimble, Thread, Sewing Needles, Tape Measure etc (Black, S)`,
        price: '$16.33',
        discount: null,
        images: '/images/images/ArtsandCrafts7.jpg'
    }
]