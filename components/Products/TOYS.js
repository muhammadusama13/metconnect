export const TOYS = [
    {
        name: "Barbie Dreamhouse (3.75-ft) 3-Story Dollhouse Playset with Pool",
        category: 'TOYS',
        Description: `Measuring 43 inches tall and 41 inches wide, the fully furnished Barbie DreamHouse inspires endless imagination with 10 indoor and outdoor play areas, customizable features and 75 Plus storytelling pieces
        Dreamy features include a working elevator, party room with a DJ booth, second-story slide with a pool, balcony with a repositionable hammock swing and a puppy play area with its own pool and slide.
        Customizations let kids design their own dream home -the grass area and pool can be placed in multiple configurations on the first and third floors, and the slide fits in 4 unique locations
        Set the scene for any story with realistic sound effects, 2 soundscapes, 3 songs, 8 light color options, and 4 light settings including day mode, night mode, party mode and mood lighting.
        A third-floor balcony and rooftop deck inspire outdoor living -bring everyone upstairs in the working elevator that can lift and lower 4 dolls or a Barbie doll in a wheelchair
        Discover wow moments on every floor with transforming pieces, like a bunk bed that folds down from the wall, a BBQ grill that reverses to reveal a dessert buffet and an entertainment center that doubles as a pet area.
        Bring detail to playtime with 75 plus storytelling pieces featuring realistic touches and textures, like plush blankets, modern décor and grass that’s soft to the touch
        Kids ages 3 years old and up can move right into the Barbie DreamHouse and discover playtime possibilities in every room. With customizable options and so many play areas, they’ll never run out of stories to tell
        `,
        price: '230',
        discount: '250  ',
        images: '/images/images/TOYS1.jpg'
    },
    {
        name: "Barbie Signature 2021 Holiday Doll (12-inch, Blonde Wavy Hair)",
        category: 'TOYS',
        Description: `2021 Holiday Barbie doll reflects the magic of the season in a gorgeous gown, sparkling with sophisticated glamour. Her elegant floor-length dress features off-the-shoulder ruched sleeves and a metallic bodice with dazzling sculpted “gemstones”
        Accented with a tulle peplum and silvery accents, her full skirt sparkles from every angle. Silvery “pearl” earrings, sparkling eye makeup and bold red lips complete her dazzling look. Barbie doll’s long, blonde hair is styled in sleek, sides wept waves.
        In holiday packaging that’s ideal for display, this collectible Barbie doll makes a great gift for 6 year olds and up
        2021 Holiday Barbie doll reflects the magic of the season in a gorgeous gown, sparkling with sophisticated glamour. Her elegant floor-length dress features off-the-shoulder ruched sleeves and a metallic bodice with dazzling sculpted “gemstones”
        Accented with a tulle peplum and silvery accents, her full skirt sparkles from every angle. Silvery “pearl” earrings, sparkling eye makeup and bold red lips complete her dazzling look. Barbie doll’s long, blonde hair is styled in sleek, sides wept waves`,
        price: '45',
        discount: '42 ',
        images: '/images/images/TOYS2.jpg'
    },
    {
        name: "Fisher-Price Imaginext DC Super Friends Bat-Tech Batbot",
        category: 'TOYS',
        Description: `
        2-in-1 Batman robot that transforms into an action-Packed playset with cool lights, sounds, and Batman character phrases. Turn the Power Pad to transform into “flight” mode, then squeeze the trigger to bring the arms forward and roll along to “blast off” for action.
Playset features multiple projectile launchers, a jail cell, and a light-up control center with a cockpit for Batman. Place Batman in the cockpit and press the button to see the figure’s chest light up.
Comes with Batman figure, 1 Batwing, 2 projectile launchers, and 7 projectiles. For preschool kids ages 3-8 years.
2-in-1 Batman robot that transforms into an action-Packed playset with cool lights, sounds, and Batman character phrases. Turn the Power Pad to transform into “flight” mode, then squeeze the trigger to bring the arms forward and roll along to “blast off” for action.
Playset features multiple projectile launchers, a jail cell, and a light-up control center with a cockpit for Batman. Place Batman in the cockpit and press the button to see the figure’s chest light up.
`,
        price: '90',
        discount: '100  ',
        images: '/images/images/TOYS3.jpg'
    },
    {
        name: "Flashing Cube Electronic Memory Flashing Cube Electronic Memory & Brain Game  4-in-1 Handheld Game for Kids",
        category: 'TOYS',
        Description: `
        FUN FOR KIDS AND ADULTS
Flashdash offers 4 quick-fingered games in a unique light-up cube design. Great for home and travel, these fun handheld games help improve brain skills and hand-eye coordination. Volume is adjustable and can be muted entirely. Requires 3x AAA batteries (not included).
SPEED GAMES
(1- Chase Me) Don’t let the moving light get out of your sight! Race it with your fingers by pressing the buttons while rotating the cube. (2- Catch Me) Quickly press the blinking red lights before time runs out while avoiding green lights and grabbing some “bonus” blue lights!
MEMORY GAMES
(3- Follow Me) Memorize and match the correct pattern of colors and lights from one side to the other to win. (4- Remember Me) Reminiscent of the classic “Simon Says”, this game flashes light sequences for you to memorize and repeat.
EASY TO LEARN, TOUGH TO MASTER
The FlashDash electronic games offer just the right amount of challenge for most people – not too easy or too hard. When you’re done playing, press the LIGHT SHOW button to enjoy a fun display of flashing lights!
MONEY BACK GUARANTEE 
We stand behind our kids video games with pride! Every piece is made with the utmost care and quality control. If you don’t love this hand held game, please contact us to get your money back.
`,
        price: '50',
        discount: null,
        images: '/images/images/TOYS4.jpg'
    },
    {
        name: "Seckton Upgrade Kids Selfie Camera",
        category: 'TOYS',
        Description: `
        Clearer Moment Recorded & Multi-scene Selections
Equipped with auto Focus, 8.0 Mega pixels and 1080P Video. Greatly improved the definition of photos . Catch the wonderful joy moment for boys or girls. With Big head sticker shooting, children can take photos with frames, leave some hilarious photos with family or friends.
Upgraded Design
Upgraded to dual-camera configuration, Easy to take selfies. Just open the switch key to enjoy the fun of taking photos. Include a portable lanyard hang, easy to carry and moving. Includes a video recorder and voice recorder for more creative ways to play.
Powerful Battery Life
Continuously taking photos for 1-2 hours after full charged! Come with 1*USB data line & 1*32GB micro SD card to store thousands of photo!
Safe & Durable Shockproof
This camera is used high quality non-toxic soft plastic, more safe and comfortable for children’s skin. Shockproof shell provides effective anti-fall protection.
Great toy for Kids
Perfect birthday, festival ,holiday gifts for boys and girls aged 3~10. Children love taking photo with extremely cute camera. Simply capture scene or animals on nature with family, experience the fun from nature. First step to develop kids as a talented photographer`,
        price: '45',
        discount: ' 42 ',
        images: '/images/images/TOYS5.jpg'
    },
    {
        name: "",
        category: 'TOYS',
        Description: ``,
        price: '120.00',
        discount: '130.00 ',
        images: '/images/images/TOYS1.jpg'
    },
]