export const babyProduct = [
    {
        name: "Baby Trend Trooper 3 in 1 Convertible Car Seat",
        category: 'Baby Products',
        Description: `
        The Baby Trend Trooper 3-in-1 Convertible car seat converts from rear-facing infant seat (4-18lbs) and rear-facing toddler seat (18-40lbs) to forward-facing toddler seat (22-65lbs) as your child grows. With 5-position shoulder height adjustment and 3- position crotch buckle for seat adjustment, the Trooper convertible gives parents the ultra-convenience for installation and adjustment. The rear-facing recline flip foot also allows the Trooper to get great installation angles with ease.
Trooper’s nimble frame can fit three across in most vehicles which is great for families that have more than one kiddo. The lightweight shell also allows for use of LATCH Installation up to 55 lbs. The super-deep side wings offer better side impact protection and premium cushion, giving your babies great head protection and support. With premium fabric and the convenient integrated cup holder on the side, Our Baby Trend Trooper 3-in-1 Convertible car seat truly gives your children The most comfortable and convenient commute/travel experience.
3 modes of use: rear facing infant mode (4-18lbs), rear facing toddler mode (18-40lbs), and forward facing toddler mode (22-65lbs)
Immediate right installation angle: rear-facing recline flip foot allows for great installation angles with ease; The built-in bubble level indicator helps find the correct angle
Fits three across: at its widest, the Trooper is 16.75″ Allowing it to fit 3 ACROSS in most vehicles
Lightweight: lightweight shell also allows for use of latch Installation up to 55 lbs
Shoulder & crotch buckle adjustment: 5-position shoulder height adjustment and 3- position crotch buckle for seat adjustment
3 modes of use: rear facing infant mode (4-18lbs), rear facing toddler mode (18-40lbs), and forward facing toddler mode (22-65lbs)
Immediate right installation angle: rear-facing recline flip foot allows for great installation angles with ease; The built-in bubble level indicator helps find the correct angle
Fits three across: at its widest, the Trooper is 16.75″ Allowing it to fit 3 ACROSS in most vehicles
Lightweight: lightweight shell also allows for use of latch Installation up to 55 lbs
`,
        price: '125.00',
        discount: '150.00 ',
        images: '/images/images/BabyProduct1.jpg'
    },
    {
        name: "Baby Wipes – Huggies Natural Care Refreshing Baby Diaper Wipe",
        category: 'Baby Products',
        Description: `
        10 flip-top packs of 56 scented baby wipes (560 wipes total); National Eczema Association Seal of Approval. Plant-Based Wipes since 1990: Huggies Natural Care Baby Wipes are plant-based & 99% purified water for a gentle clean
        Refreshing Clean: Lightly scented diaper wipes infused with cucumber & green tea for a refreshing clean; Safe for sensitive skin: hypoallergenic & dermatologically tested, wipes are pH balanced to help maintain healthy skin. No harsh ingredients: free of lotions, parabens, alcohol, dyes & elemental chlorine; does not contain phenoxyethanol or MIT.
        EZ Pull 1-Handed Dispensing: one-handed dispensing makes it easy to grab wipes without wasting sheets; packaging may vary from images shown.
        Huggies Natural Care Refreshing Baby Wipes are plant-based wipes, made with 99% purified water and 1% skin essential ingredients for a gentle clean. Infused with cucumber & green tea, the unique base sheet locks-in and absorbs the mess to help keep baby’s skin clean and moisturized. Safe for sensitive skin with no harsh ingredients, Huggies Natural Care Refreshing scented baby wipes are hypoallergenic, dermatologically tested and pH-balanced to help maintain healthy skin.
        They’re alcohol free, paraben free and do not contain phenoxyethanol or MIT, earning these diaper wipes the National Eczema Association’s seal of approval. Huggies Natural Care scented baby wipes feature Triple-Clean Layers to help get baby clean with fewer wipes.
        Plus, with EZ Pull 1-Handed Dispensing it’s easy to grab wipes without wasting sheets! With the #1 branded wipe* you can feel confident you are giving baby a safe, gentle clean every time. Join the Huggies Rewards Powered by Fetch to get rewarded fast. Earn points on Huggies diapers and wipes, in addition to thousands of other products to redeem for hundreds of gift cards. Check out the Fetch Rewards app to get started today! (*Based on US and Canada Nielsen sales ending 11/21/2020)
`,
        price: '18.00',
        discount: '22.00',
        images: '/images/images/BabyProduct2.jpg'
    },
    {
        name: "Baby Wipes, Huggies Natural Care",
        category: 'Baby Products',
        Description: `
        Contains 3 refill packs of 208 unscented baby wipes (624 wipes total).
        Pure & Gentle Wipes
        Natural Care Sensitive Baby Wipes contain 99% purified water and 1% skin essential ingredients for a gentler clean.
        Safe for Sensitive Skin
        Hypoallergenic & dermatologically tested, infused with aloe & vitamin E to keep baby’s skin healthy and moisturized.
        Hypoallergenic & Dermatologically Tested
        A gentle scented baby wipe designed to protect baby’s sensitive skin. Type: Pre moistened.
        pH Balanced – Natural Care Sensitive Baby Wipes are pH balanced to help maintain healthy skin.
        Refill bags and bulk cases are perfect for replenishing your nursery tub or Clutch ‘N’ Clean travel pouch. Do not flush. Directions: Keep lid or seal tightly closed. Store at room temperature. Packaging may vary from image shown.
        Huggies Natural Care Sensitive Baby Wipes are made with plant-based ingredients, 99% purified water and 1% skin essential ingredients to provide a gentle clean for your baby’s soft & delicate skin. Natural Care Sensitive diaper wipes are infused with aloe & vitamin E to help keep baby’s skin healthy and moisturized. They’re also hypoallergenic, dermatologically tested & pH-balanced to help maintain healthy skin.
        Natural Care unscented baby wipes are safe for sensitive skin with no harsh ingredients. They’re fragrance free, alcohol free, paraben free and do not contain phenoxyethanol or MIT. In addition, our unique base sheet absorbs, locks-in and retains the mess to help ensure your baby’s skin is clean & healthy. With the #1 branded wipe* you can feel confident you’re giving baby a safe, gentle clean every time. Don’t get caught without Huggies wipes! Sign up for Subscribe & Save to ensure you always have Huggies Natural Care Sensitive Wipes on hand.
        Join Huggies Rewards+ Powered by Fetch to get rewarded fast. Earn points on Huggies diapers and wipes, in addition to thousands of other products to redeem for hundreds of gift cards. Download the Fetch Rewards app to get started today! (*Based on US and Canada Nielsen data ending 7/20/19)`,
        price: '20.00',
        discount: '18.00',
        images: '/images/images/BabyProduct3.jpg'
    },
    {
        name: "Easy@Home 50 Ovulation Test Strips and 20 Pregnancy Test Strips Combo Kit",
        category: 'Baby Products',
        Description: `
        Specially designed for “trying to conceive” women to get pregnant naturally: These Easy@Home ovulation tests give women a greater chance of predicting their most fertile days, even if their cycle lengths vary. Pregnancy tests can tell pregnancy sooner than a missed period. The Easy@Home 50 Ovulation Test Strips and 20 Pregnancy Test Strips Kit are supported by the FREE Premom Ovulation Predictor App.
        The ovulation test strips help to track your ovulation progression, and minimize the chances of missing your LH surge. Levels of hCG as low as 25 mIU/ml (FDA cleared) can be detected in 5 minutes. Results are 99% accurate.
        Easy to test: simply dip the test in your urine until the dye rises into the result window (approximately 5 – 10 seconds).
        Easy to read: Ovulation tests — a test line as dark or darker than the control line indicates a positive result. Pregnancy tests — two lines means pregnant and one line means not pregnant.
        100% customer satisfaction guaranteed: Contact our experienced and professional customer service with any questions you may have. Refunds or returns are acceptable by brand owner, who is dedicated to being the best provider of home health tests.
        Why FDA HCG Test accuracy standard is 99% instead of 100% ? A few women may receive unexpected positive result caused by chemical pregnancy, miscarriage, blood or protein in the urine or some health condition or taking certain medicines that could elevate hCG level. Even it also rarely but it happens that some women may not receive expected positive result due to the HCG Level is not elevated as average to cut off level by missing period day. If you receive the very faint positive test line not as expected, we suggest you to wait a few more days or after missing period and test it again. If you don’t get expected positive result please wait couple of days to do another test.
`,
        price: '20.00',
        discount: '18.00',
        images: '/images/images/BabyProduct4.jpg'
    },
    

]