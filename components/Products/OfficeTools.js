export const OfficeTools = [
    {
        name: "Binder Clips Paper Clips – Sopito 300pcs Colored Office Clips",
        category: 'Office Tools',
        Description: `
        Package Includes: 50pcs binder clips (3 sizes: 15mm/0.59inch, 19mm/0.75inch, 25mm/0.98inch), 170pcs paper clips (2 sizes: 28mm/1.1inch, 50mm/1.97inch) and 80pcs general purpose rubber bands (4 colors), in total 300pcs, meets your different needs and different styles.
        Premium Materials & Economical: The binder clips are made of high-quality metal iron and their handles are processed by advanced nickel plating technology; the paper clips are made of metal iron coated with plastic; these premium materials serve the clips’ high smoothness, durability and rust resistance, made to last, plus 300pcs in total with nice price, which is much economical.
        Efficient Documents Organizer: Assorted sizes and multicolor are suitable for information labeling and documents organization of different thickness, types and scenes, decorating your busy and tired life, making you feel more relaxed and interested in the working process.
        Unlimited Application: Keep your documents, examination paper, flyer, refills, envelopes, posters and tickets well organized; also for food clamps, clothes design, furniture, photos hanging and as gifts on festivals; perfect for office, school and home daily supplies.
        Convenient to Store: The binder clips, paper clips and rubber bands set are separately stored in one transparent plastic box, which neatly saves office space and is clear to select when needed.
`,
        price: '15.00',
        discount: '12.99 ',
        images: '/images/images/OfficeTools1.jpg'
    },

    {
        name: "Colored Binder Clips – Multi Size (120 Pcs)",
        category: 'Office Tools',
        Description: `
        6 ASSORTED SIZES CLIPS: 2 inch x 2, 1.6 inch x 5, 1.25 inch x 8, 1 inch x15, 0.75 inch x 30, 0.6 inch x 60
Strong material – Our binder clips assorted sizes bulk made of tempered steel is rust-resistant, durable and reusable for a long time
Why you need It:They keep loose paper securely fastened together, which make you office more organized
Wide application – Getting these paper clips assorted sizes are ideal for instant binding groups of documents and closing open frozen veggies and bags of chips in office, home, school
Friendly Satisfaction Service: If you are not satisfied with our binder clip, please email to us and we will address the issue immediately or ask Amazon warehouse to send this item to you again
`,
        price: '13.00',
        discount: '10.99 ',
        images: '/images/images/OfficeTools2.jpg'
    },


    {
        name: "Flair Felt Tip Pens  ",
        category: 'Office Tools',
        Description: `Flair Felt Tip Pens Medium Point 0.7 Millimeter Marker Pens  School Supplies for Teachers & Students  Assorted Fashion Colors, 12 Count`,
        price: '12.46',
        discount: null,
        images: '/images/images/OfficeTools3.jpg'
    },


    {
        name: "Liquid Chalk Markers",
        category: 'Office Tools',
        Description: `Liquid Chalk Markers – Dry Erase Marker Pens – Chalk Markers for Chalkboards, Signs, Windows, Blackboard, Glass – Reversible Tip (8 Pack) – 24 Chalkboard Labels Included (Multicolored, 1mm)`,
        price: '14.88',
        discount: null,
        images: '/images/images/OfficeTools4.jpg'
    },


    {
        name: "Scissors, iBayam 8″ – Multipurpose Scissors Bulk 3-Pack",
        category: 'Office Tools',
        Description: `
        Definitely Household Scissors — Great for cutting burlap, paper, card stock, cardboard, wrapping paper, light line, fabric, tape, photos, coupons, and opening boxes & packaging. Surprised for sewing, tailoring, quilting, dressmaking, cutting patterns, cutting quilting, Vinyl, Leather, and making face masks. Exceptional scissors for multiple uses in various rooms, in the Handcraft Workshop, bathroom, sewing room, laundry room, craft room, at the office, great for most regular household tasks.
3 Pairs of Quality Straight Handled Scissors — They strongly allow you to make precise cuts with great control as the blade has the perfect amount of friction, which is very comfortable to use. 3 pairs of straight handled scissors for smooth, efficient cutting scissors for general use. Suitable for teachers’ middle/high school classroom supplies.
Sharp and Durable — Durable stainless steel blades enable high-density steel that makes it 3 times harder than normal stainless steel and is smoother cutting. 6 X longer lasting than the previous formula and remain sharp for 100,000 + cuts. Blades stay sharper longer; We chose bright colors, green/purple/orange. Different color rubber handles are used in different places, Soft-grip holes let you maintain a comfortable, can be used left- or right-handed.
iBayam 8-Inch Soft-Grip Scissors — 3 special scissors are suitable for most items that need to be cut. It can also be used as household scissors for cutting open frozen food bags ready for cooking. Convenient, sturdy, especially sharp scissors for general use, and very durable.
`,
        price: '20.99',
        discount: '24.00  ',
        images: '/images/images/OfficeTools5.jpg'
    },


    {
        name: "SimpleHouseware Mesh Desk Organizer with Sliding Drawer",
        category: 'Office Tools',
        Description: `
        Space-saving mesh design with stylish drawer. No tools needed.
Two side load letter trays, and a tray with a 3 compartment Drawer. Upright section has five 2″ Compartments.
Desk storage, file folder and letter organizer
Dimension: 13.25″L x 13″W x 9″H
USPTO Patent NO:D890,842
`,
        price: '30.99 ',
        discount: '34.00',
        images: '/images/images/OfficeTools6.jpg'
    },


    {
        name: "ZZTX Staple Remover Staple Puller Removal (Pack of 3)",
        category: 'Office Tools',
        Description: `Bulk value pack contains 3 black staple removers – Classic and dependable jaw style design
        Shiny silver metal plated jaws with sharp teeth to make sure you get even the most stubborn staples
        Removes staples easily without ripping paper
        Plastic grips are contoured for comfortable control.
        100% Satisfaction guarantee or your money back.
        `,
        price: '9.99',
        discount: '12.00',
        images: '/images/images/OfficeTools7.jpg'
    },
]