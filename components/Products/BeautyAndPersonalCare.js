export const BeautyAndPersonalCare = [
    {
        name: "Baby Registry Welcome Box",
        category: 'Beauty And Personal Care',
        Description: `
        Legal Disclaimer
Statements regarding dietary supplements have not been evaluated by the FDA and are not intended to diagnose, treat, cure, or prevent any disease or health condition.
Product Type:Grocery
Item Package Dimension :10.0 ” L X5.0 ” W X9.0 ” H
Item Package Weight: 31.0 oz
Country Of Origin: United States

`,
        price: '35.00',
        discount: '40.00',
        images: '/images/images/BeautyAndPersonalCare1.jpg'
    },



    {
        name: "Braun IPL Hair Removal for Women",
        category: 'Beauty And Personal Care',
        Description: `
        Braun Latest Generation IPL. Permanent hair reduction in just 3 months. 100-day money back guarantee. The safest IPL technology. Clinically tested and dermatologically accredited as skin safe by an international skin health organization (Skin Health Alliance)
SMART IPL with SensoAdaptTM skin sensor (with UV protection): the only IPL technology that automatically and continuously adapts to your skin tone. Fast treatment: treat both legs in just 9 minutes at the lowest energy level. 2 times faster than the previous Silk·expert 3. Includes precision head, premium bag and Venus razor
New compact design, 15 percent smaller and 25 percent lighter, for easy handling and effortless treatment. 300,000 flashes, 20 percent more than previous Silk·expert 3. Dermatologist tested: suitable for daily use, even on sensitive skin.
2 times faster treatment
You only need 9 minutes to treat both legs at the lowest energy level. The gliding mode triggers more flashes for a faster treatment, ideal for large body parts, while the stamp mode provides a more precise treatment suitable for smaller and sensitive areas.
Precise and gentle on your skin
The precision head allows you to tackle smaller or trickier areas like face, bikini and underarms. Includes two comfort modes, with a gentle setting ideal for beginners.
`,
        price: '300.00',
        discount: '270.00',
        images: '/images/images/BeautyAndPersonalCare2.jpg'
    },

    {
        name: "CeraVe Moisturizing Cream and Hydrating Face Wash Trial Comb",
        category: 'Beauty And Personal Care',
        Description: `
        MOISTURIZING CREAM & HYDRATING FACE WASH BUNDLE
Contains 12oz CeraVe Moisturizing Cream Plus a 3oz Travel Size of CeraVe Hydrating Facial Cleanser.
ESSENTIAL CERAMIDES
Making up 50% of the skin’s natural barrier, ceramides are essential to maintaining healthy skin. All CeraVe products are formulated with three essential ceramides (1, 3, 6-II) to help restore the protective skin barrier.
DRY SKIN RELIEF
A deficiency of ceramides in the skin can often be associated with dry skin, which can feel itchy and look flaky. CeraVe Moisturizing Cream was shown to increase the skin’s level of ceramides after 4 weeks.
DERMATOLOGIST RECOMMENDED
CeraVe Skincare is developed with dermatologists and has products suitable for dry skin, sensitive skin, oily skin, acne-prone, and more.
GENTLE ON SKIN
Both products included in a Bundle are free of fragrance, allergy-tested, non-comedogenic, and suitable for use on the face and/or body.
Developed with dermatologists, CeraVe Hydrating Facial Cleanser is a unique formula that cleanses, hydrates, and helps restore the protective skin barrier with three essential ceramides (1, 3, 6-II).
CeraVe Moisturizing Cream is a face & body moisturizer to lock in moisture and provide 24-hour hydration. Both formulas contain hyaluronic acid to help retain skin’s natural moisture.
•   Non-comedogenic
•   Fragrance-free
•   Gentle, non-irritating formula
Key Ingredients (both products contain):
•   MVE Technology: This patented delivery system continually releases moisturizing ingredients for 24-hour hydration
•   Ceramides: Essential for healthy skin, ceramides help restore and maintain the skin’s natural barrier
•   Hyaluronic Acid: This ingredient attracts hydration to the skin’s surface and helps the skin retain moisture


`,
        price: '16.99',
        discount: '18.99 ',
        images: '/images/images/BeautyAndPersonalCare3.jpg'
    },

    {
        name: "Dove Purely Pampering Body Wash for Dry Skin",
        category: 'Beauty And Personal Care',
        Description: `
        Pampers and softens skin:
Dove Restoring Body Wash combines Moisture Renew Blend with nourishing coconut butter and cocoa butter for soft and smooth skin, unlike ordinary body soap or shower gel.
Mild and pH-balanced:
Dove Restoring Body Wash includes Moisture Renew Blend—a combination of skin-natural nourishes and plant-based moisturizers that absorb deeply into the top layers of skin.
No.1 Dermatologist recommended body wash:
Works to restore dry skin with a rich, creamy formula, leaving your skin softer and smoother than a shower gel can.
Thoughtfully made:
This body wash for dry skin is PETA-certified cruelty-free and made in 100 percent recycled plastic bottles so you can feel good about switching from shower soap to Dove.
Plant-based moisturizer:
Naturally derived cleansers and skin-natural nutrients, Dove Restoring Body Wash is microbiome gentle, so you’ll feel beautifully nourished while maintaining healthy skin.
Care as you clean:
The cleansing efficacy and skin care you need, all in one product. Dove Body Wash is also effective at cleansing hands.
Looking for a skin cleanser that indulges your senses and pampers your skin? The No.1 dermatologist recommended body wash, Dove Restoring Body Wash nourishes your skin and senses with creamy coconut butter and cocoa butter while leaving skin soft and smooth. This body wash is sulfate- and paraben-free with a mild, pH-balanced formula, making it a great body wash for dry skin unlike typical bath soap or shower gel.
This body wash uses Dove Moisture Renew Blend, a combination of skin-natural nourishers and plant-based moisturizers. The lipids and glycerin in our formula absorb deeply into the top layers of skin, where they get right to work! This proprietary blend of moisturizing ingredients is proven to work with your skin to nourish it, so it can maintain and create new moisture.
For best results, squeeze the restoring coconut butter and cocoa butter body wash into your hand or onto a shower pouf and work it into a rich lather. Massage it over your skin, taking time to allow the nourishing coconut butter and cocoa butter to indulge your senses. Rinse off, revealing soft and smooth skin. Made with 100 percent gentle cleansers, Dove Coconut Butter and Cocoa Butter Body Wash is gentle to skin’s microbiome, its living protective layer.
It creates a rich, buttery lather that moisturizes and replenishes skin while also leaving it cleansed, smooth, and soft. Dove body wash is also effective at cleansing hands. With naturally derived cleansers and skin-natural nutrients, we care about what goes into our body wash. Dove sulfate-free body wash is No.1 dermatologist recommended and microbiome gentle.
Dove care goes further than moisturizing body wash with PETA Cruelty-Free certification and 100 percent recycled plastic bottles. At Dove, our vision is of a world where beauty is a source of confidence, and not anxiety. So, we are on a mission to help the next generation of women develop a positive relationship with the way they look—reaching over ¼ of a billion young people with self-esteem education by 2030. We are committed to a landmark new initiative as part of our 2025 commitment to reduce plastic waste—reducing over 20,500 metric tons of virgin plastic annually by making the iconic beauty bar packaging plastic-free globally, launching new 100 percent recycled plastic bottles and trialing a new refillable deodorant format that radically reduces plastic use. As one of the largest beauty brands in the world, we are revealing an agenda-setting commitment to tackle the global beauty industry’s plastic waste issue.


`,
        price: '28.00 ',
        discount: '25.00',
        images: '/images/images/BeautyAndPersonalCare4.jpg'
    },

    {
        name: "REVLON One-Step Hair Dryer",
        category: 'Beauty And Personal Care',
        Description: `
        Style, Dry & Volumize Your Hair in One Step, Max Drying Power with 30% Less Frizz and helps reduce hair damage. Unique Non-Detachable Oval Brush Design for Smoothing the Hair, while the round edges create volume. Designed with Nylon Pin & Tufted Bristles for detangling, improved volume, and control.
3 Heat/Speed Settings with Cool option for styling flexibility.
Safety First
The Revlon One Step Hair Dryer and Volumizer meets U.S. safety requirements and features the ETL Certification, unit is designed for 120 Volt USA outlets only. DO NOT use a voltage converter or adapter as it will damage the unit.
1100 Watts of Power
Note that wattage of this appliance may vary depending on the location of use.
Volumizer Care
Clean air inlet regularly, remove hair from the brush after every use and do not wrap cord around the unit. To keep bristles in best condition let unit cool before storing.
The Revlon One-Step Hair Dryer and Volumizer is a designed Hot Air Brush to deliver gorgeous volume and brilliant shine in a single step. The unique non detachable oval brush design smooths hair while the rounded edges quickly create volume at the root for beautifully full-bodied bends at the ends in a single pass. The brush is designed with Nylon Pin and Tufted Bristles for detangling, improve volume and control. Styling flexibility is guaranteed with 3 Heat/Speed Settings and a Cool Option.
Built to provide just the right amount of heat. Unlike conventional hair dryers, this volumizer can be placed closer to the scalp for lift. Boosted by Ionic Technology through a built-in genuine ION generator, hair dries fast and helps reduce heat damage. The One Step Unit is designed for 120V USA outlets only, we do not recommend use of a voltage adapter or converter, as it will damage the unit. Safety first! The Revlon One-Step Hair Dryer and Volumizer meets U.S. safety requirements and proudly features the ETL Certification seal. Directions: For faster results towel dry hair to remove excess water.
Separate hair into manageable sections. Item Weight: 1.8-pound Note: If you need 220V or for other counties – please check Amazon in that country for the correct voltage and plug.

`,
        price: '65.00',
        discount: '75.00 ',
        images: '/images/images/BeautyAndPersonalCare5.jpg'
    },
]