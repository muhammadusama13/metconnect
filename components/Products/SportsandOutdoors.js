export const SportsandOutdoors = [
    {
        name: "Crazy Skates City Series Foldable Kick Scooter",
        category: 'Sports and Out doors',
        Description: `
        SYDNEY (SYD)
The SYD Scooter from our City Series comes in 7 colors and is packed with features that make it a perfect ride for any member of the family.
FASTFOLD TECH
Our patented Fast Fold technology allows you to quickly and easily fold the scooter in half with a simple single movement.
FOLDABLE KICKSTAND
The kickstand tucks under the deck while riding and easily folds out for a simple and strong kickstand function.
HEIGHT ADJUSTABLE
The T-Bar and handlebars are height adjustable so that you can custom set the handlebars for optimal performance.
COMFORT HANDGRIPS
Our handgrips have been specially designed to be ergonomic and to offer a comfort grip over long and intense rides.
`,
        price: '85.00 ',
        discount: '75.00',
        images: '/images/images/SportsandOutdoors1.jpg'
    },

    {
        name: "Rawlings Players Series Youth Tball Baseball Gloves",
        category: 'Sports and Out doors',
        Description: `
        DESIGNED AS A UTILITY GLOVE FOR ANY POSITION, this 9 inch Players Series Left Hand Throw Youth Tall/Baseball Glove offers first-time players a lightweight and reliable option as they learn the fundamentals of defensive play. IDEAL FOR THE BEGINNING YOUTH TBALL AND BASEBALL PLAYER, AGES 3-5.
CATCHING IS MADE EASY with the soft, flexible shell and lining materials. BASKET WEB PATTERN AND SOFT SHELL for a deep pocket and Velcro straps for a custom fit. COMES WITH 1 GLOVE AND 1 SOFT RUBBER PRACTICE BALL. Right Hand Orientation = Right Hand Throw / Left Hand Orientation = Left Hand Throw.
`,
        price: '22.00 ',
        discount: '18.00',
        images: '/images/images/SportsandOutdoors2.jpg'
    },

    {
        name: "Tourna Mesh Carry Bag of 18 Tennis Balls",
        category: 'Sports and Out doors',
        Description: `
        REUSABLE MESH CARRY BAG
Tourna balls come in a convenient reusable and closeable mesh bag for easy transport and storage.
TOURNA PRESSURLESS BALLS NEVER LOSE THEIR BOUNCE
Regular tennis balls go flat over time, even if you don’t use them. These balls will have the same consistent bounce for the life of the ball.
DURABLE! Not all tennis balls are created equal
The extra durable felt on the TOURNA Pressurless tennis balls lasts a long time. Ideal for tennis ball machines, tennis practice, or even playing with your pets.
REGULATION SIZE AND BOUNCE
TOURNA tennis balls are regulation size and bounce to regulation height. Many pressureless balls are smaller than a regular ball. Play and practice with confidence on all surfaces with Tourna Pressureless Tennis Balls.
`,
        price: '25.00',
        discount: '21.99',
        images: '/images/images/SportsandOutdoors3.jpg'
    },

    {
        name: "WeSkate Scooter for Adults and Teens",
        category: 'Sports and Out doors',
        Description: `
        Dual Shock Absorbing System
This step-up pick scooter has both front and rear suspensions. And it’s more comfortable to push due to the low and wide deck
3 Height Adjustable
With 3 stages of height and high weight capacity up to 220lbs(100KG). Allows kids to grow alongside this kick scooters and also adults can use it
Smooth and Fast Ride
Its wheels are the inline style at 200mm with high durability PU material. Big wheels are found to be more comfortable, smoother and reduce vibration
Quick Stopping
Rear Fender Brake for emergency and quick stops. Mudguard in the rear wheel helps prevent you from getting sprayed if you go through a puddle
Three-Action Folding Mechanism
Easily to fold in a compact size. This foldable kick scooter comes with a carrying strap making it convenient to take with you on the go`,
        price: '105.00 ',
        discount: '99.99',
        images: '/images/images/SportsandOutdoors4.jpg'
    },



] 