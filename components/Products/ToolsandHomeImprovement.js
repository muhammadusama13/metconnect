export const ToolsandHomeImprovement = [
    {
        name: "Holikme 4Pack Drill Brush Power Scrubber Cleaning Brush",
        category: 'Tools and Home Improvement',
        Description: `
        3 shape of brushes clean your bathtub, grout, upholstery, bathroom surface, floor, tile, shower, toilet and carpet etc, Nylon bristles will not scratch surfaces of them.
        Extended reach attachment help brush to clean the tight spaces or hard-to-reach areas such like stove grates, drip pans, crevices, car etc
        Extended reach attachment have the slip joint and magnet， it can lock the brushes tightly and make precise control.
        Bruehes and Extended reach attachment fit into most of the cordless drills
        Drill NOT included.        
        `,
        price: '12.99',
        discount: '15.00 ',
        images: '/images/images/ToolsandHomeImprovement1.jpg'
    },
    {
        name: "Komelon SL2825 Tape",
        category: 'Tools and Home Improvement',
        Description: `
        The Self Lock tape measure series by Komelon has offers a unique design that allows the blade to extend smoothly and with the push of a button retract the blade with complete control. You can find this tape measure in additional sizes, and engineer scale. This is the perfect measuring tool for anyone from the do it yourselfer to the professional tradesmen.
No more fumbling around with locking mechanisms. Pull out the blade and it locks itself, retract the blade with the simple push of a button.
416-SL2825 Features: -Self locking mechanism allows blade to be extended smoothly. -Push button allows blade to be retracted smoothly. -Nylon coated blade for maximum durability. -Impact resistant rubberized case. Product Type: -Tapes. Dimensions: Overall Product Weight: -0.83 Pounds.
From the Manufacturer
The Self Lock Series of tape measures is a unique line of tape measures. The series features 12-foot x 5/8-inch, 16-foot x 3/4-inch, 16-foot x 1-inch, 25-foot x 1-inch, 25-foot x 1-inch inch/Engineer scale. The Self Lock Series features a unique self locking mechanism that allows the blade to extend smoothly as well as a push button release that insures complete control when retracting the blade. This is the ideal measuring tool for anyone from the do it yourselfer to a professional tradesmen. The series also provides an impact resistant rubberized case and a nylon coated blade for maximum durability.

       `,
        price: '18.00',
        discount: '15.00',
        images: '/images/images/ToolsandHomeImprovement2.jpg'
    },
    {
        name: "Little Giant Ladders",
        category: 'Tools and Home Improvement',
        Description: `
        Multi-position ladder converts to A-frame, extension, trestle-and-plank, 90-degree and staircase with ease.
Wide-flared Ratchet leg levelers adjust to reach uneven terrain in seconds.
Rock Lock adjusters quickly alter your ladder into different configurations.
Tip & Glide wheels for easy transport from job to job.
Type IA ladder is rated to hold 300 lbs.

       `,
        price: '350.00',
        discount: '320.00',
        images: '/images/images/ToolsandHomeImprovement3.jpg'
    },

    {
        name: "Moen 26112SRN Engage Magnetix Six-Function 5.5-Inch Handheld Showerhead",
        category: 'Tools and Home Improvement',
        Description: `
        SPOT RESISTANT:
        Spot Resist Nickel finish resists fingerprints and water spots for a cleaner looking bath
        MAGNETIC BASE:
        The magnetic docking system allows you to easily detach and use as a handheld shower head or effortlessly replace it to dock with the snap of a magnet. Connection Type: IPS
        CUSTOMIZE YOUR SHOWER:
        Multiple distinct shower settings give flexibility and variety including massage, relaxing massage, wide coverage, rinse, downpour, and intensify
        FLEXIBLE:
        Features a kink-free metal hose that extends reach and flexibility
        
       `,
        price: '120.00',
        discount: '130.00 ',
        images: '/images/images/ToolsandHomeImprovement4.jpg'
    },
    {
        name: "Pro Grade Paint Brushes 5 Ea Paint Brush Set",
        category: 'Tools and Home Improvement',
        Description: `
        Paint Brush Set Includes 1 Ea of 1″ Flat, 1-1/2″ Angle, 2″ Stubby Angle, 2″ Flat & 2-1/2″ Angle
Paint Brushes For Interior Or Exterior Projects. Use Angle Sash Paint Brush For Cutting Crisp Lines.
Professional Results On Walls, Trim, Cabinets, Doors, Fences, Decks, Touch Ups, Arts And Crafts.
Premium Quality Paint Brushes At A Cheap Price! No Cardboard Covers Come With This Set.
The SRT filament blend holds more paint to save time, less streaks & premium finish on walls & trim.
`,
        price: '25.00',
        discount: '22.00',
        images: '/images/images/ToolsandHomeImprovement5.jpg'
    },
]