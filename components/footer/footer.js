import React from "react";

const Footer = () => {
  return (
    <footer className="bg-whiteghost mt-20 py-14">
      <section className="customcontainer mx-auto">
        <div className=" sm:text-center">
          <img src="images/logo.svg " className="inline-block" />
          <div className="flex sm:justify-center mt-30px">
            <img src="images/ind.svg" className="mx-1.5" />
            <img src="images/fb.svg" className="mx-1.5" />
            <img src="images/goo.svg" className="mx-1.5" />
            <img src="images/pin.svg" className="mx-1.5" />
            <img src="images/tiw.svg" className="mx-1.5" />
          </div>
          <p className="text-litegray text-sm mt-5">
            Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam
            nonummy nibh euismod <br /> tincidunt ut laoreet dolore Ut wisi enim
            ad minim veniam, quis nostrud exerci tation ullamcorper
          </p>
        </div>
        <div className="sm:flex mt-14 justify-between ">
          <div className="flex self-center mb-4">
            <img src="images/loc.svg" />
            <p className="text-litegray text-sm  ml-1">
              237 7th SE Avenue, New York
            </p>
          </div>
          <div className="flex self-center mb-4">
            <img src="images/call.svg" />
            <p className="text-litegray text-sm  ml-1">Phone : (+1) 345 6789</p>
          </div>
          <div className="flex self-center mb-4">
            <img src="images/email.svg" />
            <p className="text-litegray text-sm  ml-1">
              Email : Example@companyname
            </p>
          </div>
        </div>
      </section>
    </footer>
  );
};

export default Footer;
