import React, { useState, Fragment } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { Menu, Transition } from "@headlessui/react";

const Navbar = () => {
  const [menu, setmenu] = useState(false);
  const router = useRouter();
  return (
    <>
      <header className="bg-whiteghost hidden lg:block">
        <div className="customcontainer mx-auto h-45px flex items-center justify-between ">
          <div>
            <img src="images/logo.svg" className="order-1 sm:order-none" />

          </div>
          <div className="flex">
            <div className="flex mr-48px">
              <img src="images/mail.svg" />
              <p className="text-litegray text-sm ml-2">Example@companyname</p>
            </div>
            <div className="flex mr-48px">
              <img src="images/phone.svg" />
              <p className="text-litegray text-sm ml-2">+1 345 6789</p>
            </div>
            <div className="flex mr-48px">
              <img src="images/map.svg" />
              <p className="text-litegray text-sm ml-2">
                237 7th SE Avenue, New York
              </p>
            </div>
          </div>
        </div>
      </header>
      <nav className="customcontainer mx-auto flex h-70px justify-between items-center flex-row-reverse sm:flex-row">
        <button
          className="block lg:hidden order-1 sm:order-none"
          onClick={() => setmenu(!menu)}
        >
          <img src="images/bar.svg" />
        </button>
        <div
          className={`${menu ? "menu_show" : "menu_hide"
            }  transition-all duration-200 overflow-y-scroll md:overflow-visible fixed bg-white lg:bg-transparent left-0 top-0 h-full w-full z-50  lg:h-8 lg:w-max lg:static`}
        >
          <button
            className="ml-auto block mr-4 my-3 lg:hidden"
            onClick={() => setmenu(!menu)}
          >
            <img src="images/cross.svg" />
          </button>
          <ul className="lg:flex px-4 lg:px-0">
            <li className="mr-50px py-3 lg:py-0">
              <Link href="/">
                <a
                  className={`${router.pathname == "/" ? "active_nav text-orange" : ""
                    } text-litegray text-sm uppercase font-semibold`}
                >
                  Home
                </a>
              </Link>
            </li>
            <li className="mr-50px py-3 lg:py-0">
              <Link href="/shop">
                <a
                  className={`${router.pathname == "/shop" ? "active_nav text-orange" : ""
                    } text-litegray text-sm uppercase font-semibold`}
                >
                  shop
                </a>
              </Link>
            </li>
            <li className="mr-50px py-3 lg:py-0">
              <Link href="/shop">
                <a
                  className={`${router.pathname == "/shop" ? "active_nav text-orange" : ""
                    } text-litegray text-sm uppercase font-semibold`}
                >
                  All category
                </a>
              </Link>
            </li>
            <li className="mr-50px py-3 lg:py-0">
              <Link href="/shop">
                <a
                  className={`${router.pathname == "/shop" ? "active_nav text-orange" : ""
                    } text-litegray text-sm uppercase font-semibold`}
                >
                  Baby Products
                </a>
              </Link>
            </li>
            <li className="mr-50px py-3 lg:py-0">
              <Link href="/shop">
                <a
                  className={`${router.pathname == "/shop" ? "active_nav text-orange" : ""
                    } text-litegray text-sm uppercase font-semibold`}
                >
                  Arts and Crafts
                </a>
              </Link>
            </li>
            <li className="mr-50px py-3 lg:py-0">
              <Link href="/shop">
                <a
                  className={`${router.pathname == "/shop" ? "active_nav text-orange" : ""
                    } text-litegray text-sm uppercase font-semibold`}
                >
                  Beauty And Personal Care
                </a>
              </Link>
            </li>
            {/* <li className="mr-50px py-3 lg:py-0">
              <Link href="/shop">
                <a
                  className={`${router.pathname == "/shop" ? "active_nav text-orange" : ""
                    } text-litegray text-sm uppercase font-semibold`}
                >
                  Office Tools
                </a>
              </Link>
            </li> */}
            <li className="mr-50px py-3 lg:py-0">
              <Link href="/shop">
                <a
                  className={`${router.pathname == "/shop" ? "active_nav text-orange" : ""
                    } text-litegray text-sm uppercase font-semibold`}
                >
                  Sports and Out doors
                </a>
              </Link>
            </li>
          </ul>


        </div>

      </nav>
    </>
  );
};

export default Navbar;
