import React, { useState, useEffect, useRef } from "react";
import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Card from "../components/productcard/card";

const Productdetails = () => {
  const Prev = ({ onClick }) => {
    return (
      <button
        type="button"
        data-role="none"
        className="slick-arrow slick-prev"
        onClick={onClick}
      >
        <img src="images/left.svg" className="transform rotate-90" />
      </button>
    );
  };
  const Next = ({ onClick }) => {
    return (
      <button
        type="button"
        data-role="none"
        className="slick-arrow slick-next"
        onClick={onClick}
      >
        <img src="images/left.svg" className="transform -rotate-90" />
      </button>
    );
  };
  const [nav1, setNav1] = useState(null);
  const [nav2, setNav2] = useState(null);
  const slider4 = useRef(null);
  const slider5 = useRef(null);
  useEffect(() => {
    setNav1(slider4.current);
    setNav2(slider5.current);
  }, []);
  const [tab, setTab] = useState("tab1");
  const product = [
    {
      name: "Beautiful Chair made of wood",
      img: "images/img1.png",
      discount: true,
      discountprice: "$22",
      price: "$34",
    },
    {
      name: "Soft Chairs with Foam Cover",
      img: "images/img2.png",
      discount: false,
      discountprice: "$22",
      price: "$34",
    },
    {
      name: "Rounded Chairs",
      img: "images/img3.png",
      discount: false,
      discountprice: "$22",
      price: "$34",
    },
    {
      name: "Beautiful Art Chair",
      img: "images/img4.png",
      discount: false,
      discountprice: "$22",
      price: "$34",
    },
  ];
  const todo = [0, 1, 3];
  return (
    <>
      <section className="bg_banner">
        <div className="w-full h-full banner_heading">
          <h1 className="text-darkblack">Product Details</h1>
        </div>
      </section>
      <section className="customcontainer mx-auto mt-20 sm:flex">
        <div className="w-full sm:w-1/2 sm:flex flex-row-reverse ">
          <div className="w-full sm:w-4/5 ">
            <Slider
              arrows={false}
              asNavFor={nav2}
              ref={slider4}
              infinite={false}
              className="items_slider"
            >
              <div>
                <img src="images/sofa.png" />
              </div>
              <div>
                <img src="images/sofa.png" />
              </div>
              <div>
                <img src="images/sofa.png" />
              </div>
              <div>
                <img src="images/sofa.png" />
              </div>
              <div>
                <img src="images/sofa.png" />
              </div>
            </Slider>
          </div>
          <div className="w-full sm:w-1/5 self-center">
            <Slider
              vertical={true}
              verticalSwiping={true}
              asNavFor={nav1}
              ref={slider5}
              slidesToShow={4}
              swipeToSlide={true}
              focusOnSelect={true}
              arrows={true}
              nextArrow={<Next />}
              prevArrow={<Prev />}
              responsive={[
                {
                  breakpoint: 768,
                  settings: {
                    arrows: false,
                    vertical: false,
                    slidesToShow: 3,
                  },
                },
                {
                  breakpoint: 600,
                  settings: {
                    arrows: false,
                    vertical: false,
                    slidesToShow: 3,
                  },
                },
              ]}
              className="nav_slider"
            >
              <div>
                <img src="images/lsofa1.png" />
              </div>
              <div>
                <img src="images/lsofa1.png" />
              </div>
              <div>
                <img src="images/lsofa1.png" />
              </div>
              <div>
                <img src="images/lsofa1.png" />
              </div>
              <div>
                <img src="images/lsofa1.png" />
              </div>
            </Slider>
          </div>
        </div>
        <div className="w-full sm:w-1/2 pl-0 sm:pl-30px">
          <h1 className="font25 font-bold text-darkblack ">
            Study Table and Chair
          </h1>
          <div className="flex mt-6">
            <img src="images/fstar.svg" className="mr-1" />
            <img src="images/fstar.svg" className="mr-1" />
            <img src="images/fstar.svg" className="mr-1" />
            <img src="images/estar.svg" className="mr-1" />
            <img src="images/estar.svg" className="mr-1" />
            <p className="text-darkwhite text-sm ">( 1 Customer Review )</p>
          </div>
          <div className="flex mt-6 items-center">
            <p className="line-through text-base text-darkblack mr-4">$50 </p>
            <p className=" text-lg font-semibold text-orange">$35.00</p>
          </div>
          <p className="text-sm mt-6 text-darkblack">
            Cum sociis natoque penatibus et magnis dis parturient montes,
            nascetur ridiculus mus. Nunc finibus sit amet ligula id gravida.
            Curabitur quis orci non leo varius dapibus in ornare
            tortorparturient montes.
          </p>
          <div className="mt-6 flex">
            <div className="border w-77px h-50px flex items-center justify-around text-darkblack text-base">
              <button>-</button>
              <span>01</span>
              <button>+</button>
            </div>
            <button className="bg-orange text-white text-lg font-semibold h-50px w-217px ml-4">
              ADD TO CART
            </button>
            <button className="border w-50px h-50px flex items-center justify-center  ml-4">
              <img src="images/heart.svg" />
            </button>
          </div>
          <div className="flex mt-6">
            <p className="text-darkblack text-sm mr-10">Color:</p>
            <button className="border h-5 w-5  p-0.5  mx-1">
              <span className="bg-black h-full w-full block "></span>
            </button>
            <button className="border h-5 w-5  p-0.5  mx-1">
              <span className="bg-litegray h-full w-full block "></span>
            </button>
            <button className="border h-5 w-5  p-0.5  mx-1">
              <span className="bg-gray-400 h-full w-full block "></span>
            </button>
          </div>
          <div className="flex mt-6">
            <p className="text-darkblack text-sm mr-10">Category:</p>
            <p className="text-litegray text-sm">Sofas</p>
          </div>
          <div className="soical_icon flex items-center mt-6 border-t-2 pt-7">
            <div>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="26"
                height="26"
                viewBox="0 0 26 26"
              >
                <g
                  id="Layer_2"
                  data-name="Layer 2"
                  transform="translate(0.152)"
                >
                  <g id="invisible_box" data-name="invisible box">
                    <rect
                      id="Rectangle_30"
                      data-name="Rectangle 30"
                      width="26"
                      height="26"
                      transform="translate(-0.152)"
                      fill="none"
                    />
                    <rect
                      id="Rectangle_31"
                      data-name="Rectangle 31"
                      width="26"
                      height="26"
                      transform="translate(-0.152)"
                      fill="none"
                    />
                  </g>
                  <g
                    id="icons_Q2"
                    data-name="icons Q2"
                    transform="translate(4.01 4.098)"
                  >
                    <path
                      id="Path_8"
                      data-name="Path 8"
                      d="M20.6,4.1H5.346A1.3,1.3,0,0,0,4,5.4V20.7A1.3,1.3,0,0,0,5.346,22H20.6a1.3,1.3,0,0,0,1.3-1.3V5.4a1.3,1.3,0,0,0-1.3-1.3ZM9.339,19.354H6.647V10.829H9.339ZM7.993,9.663a1.57,1.57,0,0,1-1.525-1.57,1.525,1.525,0,1,1,3.051,0,1.57,1.57,0,0,1-1.525,1.57Zm11.26,9.691H16.606V15.181c0-.987,0-2.243-1.391-2.243s-1.57,1.077-1.57,2.2v4.217H11V10.829h2.512V12h.045A2.781,2.781,0,0,1,16.068,10.6c2.692,0,3.185,1.75,3.185,4.083Z"
                      transform="translate(-4 -4.098)"
                    />
                  </g>
                </g>
              </svg>
            </div>
            <div>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="26"
                height="26"
                viewBox="0 0 26 26"
              >
                <g
                  id="Layer_2"
                  data-name="Layer 2"
                  transform="translate(0.038)"
                >
                  <g id="invisible_box" data-name="invisible box">
                    <rect
                      id="Rectangle_23"
                      data-name="Rectangle 23"
                      width="26"
                      height="26"
                      transform="translate(-0.038)"
                      fill="none"
                    />
                    <rect
                      id="Rectangle_24"
                      data-name="Rectangle 24"
                      width="26"
                      height="26"
                      transform="translate(-0.038)"
                      fill="none"
                    />
                  </g>
                  <g
                    id="icons_Q2"
                    data-name="icons Q2"
                    transform="translate(4.051 4.1)"
                  >
                    <path
                      id="Path_5"
                      data-name="Path 5"
                      d="M22,5V21.058a.134.134,0,0,1-.045.09.854.854,0,0,1-.81.808H16.42V15.046h2.2c.135,0,.135,0,.18-.135l.135-1.122c.045-.449.09-.9.18-1.346s0-.135-.09-.135H16.42V10.56c0-.135.045-.314.045-.449a.809.809,0,0,1,.72-.718l.585-.09h1.35c.09,0,.09,0,.09-.09V6.926c0-.045,0-.09-.09-.09H17.05a3.248,3.248,0,0,0-1.485.314A2.7,2.7,0,0,0,13.99,8.541a3.267,3.267,0,0,0-.36,1.615c-.045.673,0,1.391,0,2.109h-2.3c-.09,0-.09.045-.09.09v2.557c0,.09,0,.09.09.09h2.3V21.91a.134.134,0,0,0-.045.09H4.855a.854.854,0,0,1-.81-.808C4,21.192,4,21.148,4,21.1V5c0-.045,0-.09.045-.09a.854.854,0,0,1,.81-.808h16.29a.854.854,0,0,1,.81.808Z"
                      transform="translate(-4 -4.1)"
                    />
                  </g>
                </g>
              </svg>
            </div>
            <div>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="26"
                height="26"
                viewBox="0 0 26 26"
              >
                <g
                  id="Layer_2"
                  data-name="Layer 2"
                  transform="translate(0.076)"
                >
                  <g id="invisible_box" data-name="invisible box">
                    <rect
                      id="Rectangle_28"
                      data-name="Rectangle 28"
                      width="26"
                      height="26"
                      transform="translate(-0.076)"
                      fill="none"
                    />
                    <rect
                      id="Rectangle_29"
                      data-name="Rectangle 29"
                      width="26"
                      height="26"
                      transform="translate(-0.076)"
                      fill="none"
                    />
                  </g>
                  <g
                    id="icons_Q2"
                    data-name="icons Q2"
                    transform="translate(4.746 4)"
                  >
                    <path
                      id="Path_7"
                      data-name="Path 7"
                      d="M13.5,11.425v3.42h4.782a4.8,4.8,0,0,1-4.782,3.6,5.328,5.328,0,0,1-5.075-3.91,5.514,5.514,0,0,1,2.207-6.1,5.2,5.2,0,0,1,6.333.43l2.457-2.52A8.6,8.6,0,0,0,8.9,5.349a9.108,9.108,0,0,0-3.821,10.1A8.81,8.81,0,0,0,13.5,22c7.37,0,8.993-7.065,8.291-10.575Z"
                      transform="translate(-4.746 -4)"
                    />
                  </g>
                </g>
              </svg>
            </div>
            <div>
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="26"
                height="26"
                viewBox="0 0 26 26"
              >
                <g
                  id="Layer_2"
                  data-name="Layer 2"
                  transform="translate(0.114)"
                >
                  <g id="invisible_box" data-name="invisible box">
                    <rect
                      id="Rectangle_25"
                      data-name="Rectangle 25"
                      width="26"
                      height="26"
                      transform="translate(-0.114)"
                      fill="none"
                    />
                    <rect
                      id="Rectangle_26"
                      data-name="Rectangle 26"
                      width="26"
                      height="26"
                      transform="translate(-0.114)"
                      fill="none"
                    />
                    <rect
                      id="Rectangle_27"
                      data-name="Rectangle 27"
                      width="26"
                      height="26"
                      transform="translate(-0.114)"
                      fill="none"
                    />
                  </g>
                  <g
                    id="icons_Q2"
                    data-name="icons Q2"
                    transform="translate(3.009 2.956)"
                  >
                    <path
                      id="Path_6"
                      data-name="Path 6"
                      d="M3,12.988a9.731,9.731,0,0,1,3-7.1,9.952,9.952,0,0,1,17,7.1,9.778,9.778,0,0,1-3,7.1A9.9,9.9,0,0,1,13,23a9.562,9.562,0,0,1-2.81-.429,4.912,4.912,0,0,0,.524-1,13.359,13.359,0,0,0,.81-2.479c.048-.334.19-.81.333-1.383a2.43,2.43,0,0,0,.952.858,3.949,3.949,0,0,0,3.714-.143,6.244,6.244,0,0,0,2.81-3.623,7.54,7.54,0,0,0-.19-4.815A5.623,5.623,0,0,0,16,6.791a8.753,8.753,0,0,0-5.524.191,6.241,6.241,0,0,0-3.762,3.433,7.683,7.683,0,0,0-.333,1.764,10.023,10.023,0,0,0,.048,1.716,3.339,3.339,0,0,0,.619,1.526,2.906,2.906,0,0,0,1.238,1,.285.285,0,0,0,.286,0A1,1,0,0,0,8.9,15.8a1.527,1.527,0,0,0,.1-.715.429.429,0,0,0-.143-.286,3.913,3.913,0,0,1-.524-2.813A4.674,4.674,0,0,1,9.523,9.508,5.235,5.235,0,0,1,12.9,8.126,4.235,4.235,0,0,1,16.19,9.175a3.911,3.911,0,0,1,.952,2.05,6.4,6.4,0,0,1,0,2.336,6.536,6.536,0,0,1-.762,2.1A2.953,2.953,0,0,1,14.1,17.231a1.856,1.856,0,0,1-1.286-.62,1.336,1.336,0,0,1-.286-1.335A12.978,12.978,0,0,1,13,13.608a11.834,11.834,0,0,0,.429-1.859c-.1-1-.524-1.526-1.381-1.526a1.809,1.809,0,0,0-1.429.858,3.626,3.626,0,0,0-.524,1.764,4.151,4.151,0,0,0,.381,1.573c-.286,1.144-.524,2.1-.667,2.813L9.381,18.9a16.606,16.606,0,0,0-.429,2.145v1.1a10.289,10.289,0,0,1-4.334-3.719A9.59,9.59,0,0,1,3,12.988Z"
                      transform="translate(-2.999 -2.956)"
                    />
                  </g>
                </g>
              </svg>
            </div>
            <div className="tiwwter_icon">
              <svg
                id="icons_Q2"
                data-name="icons Q2"
                xmlns="http://www.w3.org/2000/svg"
                width="22"
                height="17.099"
                viewBox="0 0 22 17.099"
              >
                <path
                  id="Path_4"
                  data-name="Path 4"
                  d="M24,8.037a9.486,9.486,0,0,1-2.6.716,4.53,4.53,0,0,0,2-2.386,11.942,11.942,0,0,1-2.85,1.05A4.7,4.7,0,0,0,15.6,6.358a4.355,4.355,0,0,0-2.9,3.969,3.472,3.472,0,0,0,.15,1,13.12,13.12,0,0,1-9.3-4.485A3.92,3.92,0,0,0,2.9,8.991,4.2,4.2,0,0,0,4.95,12.57a4.2,4.2,0,0,1-2.05-.525h0a4.3,4.3,0,0,0,3.6,4.246,4.488,4.488,0,0,1-1.2.191l-.85-.1A4.58,4.58,0,0,0,8.7,19.393a9.386,9.386,0,0,1-5.6,1.813H2a12.988,12.988,0,0,0,6.9,1.956A12.476,12.476,0,0,0,21.75,10.9v-.62A8.926,8.926,0,0,0,24,8.037Z"
                  transform="translate(-2 -6.063)"
                />
              </svg>
            </div>
          </div>
        </div>
      </section>
      <section className="customcontainer mx-auto mt-20">
        <div className="bg-gray-100 flex h-50px">
          <button
            className={`flex-1 text-sm font-semibold transition transtion 
            ${tab == "tab1" ? "bg-orange text-white" : "text-darkblack"} `}
            onClick={() => setTab("tab1")}
          >
            Description
          </button>
          <button
            className={`flex-1 text-sm font-semibold transition transtion  
            ${tab == "tab2" ? "bg-orange text-white" : "text-darkblack"} `}
            onClick={() => setTab("tab2")}
          >
            Information
          </button>
          <button
            className={`flex-1 text-sm font-semibold transition transtion  
            ${tab == "tab3" ? "bg-orange text-white" : "text-darkblack"} `}
            onClick={() => setTab("tab3")}
          >
            Reviews
          </button>
        </div>
        <div className="mt-7">
          {tab == "tab1" && (
            <div>
              <p className="text-litegray text-sm mb-6">
                Lorem Ipsum is simply dummy text of the printing and typesetting
                industry. Lorem Ipsum has been the industry's standard dummy
                text ever since the 1500s, when an unknown printer took a galley
                of type and scrambled it to make a type specimen book. It has
                survived not only five centuries, but also the leap into
                electronic typesetting, remaining essentially unchanged. It was
                popularised in the 1960s with the release of Letraset sheets
              </p>
              <p className="text-litegray text-sm mb-6">
                containing Lorem Ipsum passages, and more recently with desktop
                publishing software like Aldus PageMaker including versions of
                Lorem Ipsum. When an unknown printer took a galley of type and
                scrambled it to make a type specimen book. It has survived not
                only five centuries, but also the leap into electronic
                typesetting,{" "}
              </p>
              <p className="text-litegray text-sm mb-6">
                remaining essentially unchanged. It was popularised in the 1960s
                with the release of Letraset sheets containing Lorem Ipsum
                passages, and more recently with desktop publishing software
                like Aldus PageMaker including versions of Lorem Ipsum.
              </p>
            </div>
          )}
          {tab == "tab2" && (
            <table className="border w-full">
              <tr>
                <th className="w-1/5 border p-4 text-black">Colour</th>
                <td className="w-4/5 border p-4 text-litegray text-sm">
                  Dark Grey, Light Grey , Black, White
                </td>
              </tr>
              <tr>
                <th className="w-1/5 border p-4 text-black">Weight</th>
                <td className="w-4/5 border p-4 text-litegray text-sm">
                  7.5 KG
                </td>
              </tr>
              <tr>
                <th className="w-1/5 border p-4 text-black">Meterial</th>
                <td className="w-4/5 border p-4 text-litegray text-sm">
                  Wood , Pure Leather and Foam
                </td>
              </tr>
              <tr>
                <th className="w-1/5 border p-4 text-black">Washable</th>
                <td className="w-4/5 border p-4 text-litegray text-sm">NO</td>
              </tr>
            </table>
          )}
          {tab == "tab3" && (
            <>
              <p className="text-litegray text-base">
                There are 3 reviews for “Study Table and Chair”
              </p>
              <div className="mt-50px">
                {todo.map((ele, index) => {
                  return (
                    <div className="border-b py-4 sm:flex" key={index}>
                      <div>
                        <img
                          src="images/re1.png"
                          className="w-20 h-20 object-cover object-top"
                        />
                      </div>
                      <div className="sm:pl-6">
                        <div className="flex justify-between items-center">
                          <div className="flex items-center">
                            <p className="text-darkblack font-bold sm">
                              Aamir Khan
                            </p>
                            <p className="text-gray-300 text-xs ml-8">
                              December 01, 2021
                            </p>
                          </div>
                          <div className="flex ">
                            <img src="images/fstar.svg" />
                            <img src="images/fstar.svg" />
                            <img src="images/fstar.svg" />
                            <img src="images/estar.svg" />
                            <img src="images/estar.svg" />
                          </div>
                        </div>
                        <p className="text-litegray text-sm mt-4">
                          Lorem Ipsum is simply dummy text of the printing and
                          typesetting industry. Lorem Ipsum has been the
                          industry's standard dummy text ever since the 1500s,
                          when an unknown printer.
                        </p>
                      </div>
                    </div>
                  );
                })}
              </div>
              <div className="mt-14">
                <h1 className="text-darkblack text-lg font-bold review_title pb-1">
                  ADD A REVIEW
                </h1>
                <div className="flex mt-10">
                  <p className="text-sm text-darkblack font-bold">
                    YOUR RATING
                  </p>
                  <div className="flex ml-6">
                    <img src="images/fstar.svg" />
                    <img src="images/fstar.svg" />
                    <img src="images/fstar.svg" />
                    <img src="images/estar.svg" />
                    <img src="images/estar.svg" />
                  </div>
                </div>
                <form className="mt-30px sm:flex justify-between flex-wrap">
                  <div>
                    <label className="text-darkblack text-sm font-bold block">
                      Name *
                    </label>
                    <input
                      type="text"
                      className="w-full border h-40px sm:w-340px lg:w-470px mt-4 text-sm text-litegray pl-3"
                      placeholder="Enter your name"
                    />
                  </div>
                  <div>
                    <label className="text-darkblack text-sm font-bold block">
                      Email *
                    </label>
                    <input
                      type="text"
                      className="w-full border h-40px sm:w-340px lg:w-470px mt-4 text-sm text-litegray pl-3"
                      placeholder="Enter your email"
                    />
                  </div>
                  <div className="w-full mt-6">
                    <label className="text-darkblack text-sm font-bold block">
                      YOUR REVIEW *
                    </label>
                    <textarea
                      type="text"
                      className="border resize-none h-139px w-full mt-4 text-sm text-litegray p-3"
                    />
                  </div>
                  <button
                    className="bg-black text-white font-semibold text-base w-full h-45px mt-30px "
                    onClick={(e) => e.preventDefault()}
                  >
                    SUBMIT
                  </button>
                </form>
              </div>
            </>
          )}
        </div>
      </section>
      <section className="customcontainer mx-auto mt-14">
        <div className="text-center">
          <h1 className="text-darkblack font25 font-semibold ">
            RELATED PRODUCTS
          </h1>
          <img src="images/line.svg" className="inline-block" />
        </div>
        <div className="sm:flex sm:flex-wrap justify-between mt-6">
          {product.map((ele, index) => {
            return (
              <Card
                key={index}
                className="w-217px mx-auto sm:mx-0 sm:w-161px lg:w-217px "
                img={ele.img}
                name={ele.name}
                discount={ele.discount}
                discountprice={ele.discountprice}
                price={ele.price}
              />
            );
          })}
        </div>
      </section>
    </>
  );
};

export default Productdetails;
