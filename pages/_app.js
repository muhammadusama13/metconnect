import "../styles/main.scss";
import "../styles/globals.css";
import "../styles/font.scss";
import Navbar from "../components/navbar/navbar";
import Footer from "../components/footer/footer";

function MyApp({ Component, pageProps }) {
  return (
    <>
      <Navbar />
      <Component {...pageProps} />
      <Footer />
    </>
  );
}

export default MyApp;
