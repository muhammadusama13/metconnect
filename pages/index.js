import Slider from "react-slick";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Card from "../components/productcard/card";
import { useEffect } from "react";
import { FeaturedProducts } from "../components/Products/FeaturedProducts";
import { babyProduct } from "../components/Products/BabyProducts";
import { BeautyAndPersonalCare } from "../components/Products/BeautyAndPersonalCare";
import { OfficeTools } from "../components/Products/OfficeTools";
import { SportsandOutdoors } from "../components/Products/SportsandOutdoors";
import { ToolsandHomeImprovement } from "../components/Products/ToolsandHomeImprovement";
import { TOYS } from "../components/Products/TOYS";
import Link from "next/link";


export default function Home() {
  const Prevarrow = ({ onClick }) => {
    return (
      <button
        type="button"
        data-role="none"
        className="slick-arrow slick-prev"
        onClick={onClick}
      >
        <img src="images/left.svg" />
      </button>
    );
  };
  const Nextarrow = ({ onClick }) => {
    return (
      <button
        type="button"
        data-role="none"
        className="slick-arrow slick-next"
        onClick={onClick}
      >
        <img src="images/left.svg" className="transform__rotate" />
      </button>
    );
  };
  var silder1 = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: false,
    arrows: true,
    nextArrow: <Nextarrow />,
    prevArrow: <Prevarrow />,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          arrows: false,
        },
      },
    ],
  };
  const productslider = {
    dots: false,
    infinite: true,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
    draggable: false,
    nextArrow: <Nextarrow />,
    prevArrow: <Prevarrow />,
    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 3,
          // slidesToScroll: ,
        },
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          arrows: false,
          // centerMode: true,
          // slidesToScroll: ,
        },
      },
    ],
  };

  const sliderProduct = [
    {
      name: "Baby Trend Trooper 3 in 1 Convertible Car Seat",
      category: 'Baby Products',
      path: '/baby_products',
      images: '/images/images/BabyProduct1.jpg'
    },
    {
      name: "Baby Registry Welcome Box",
      category: 'Beauty And Personal Care',
      path: 'beauty_and_personal_care',
      images: '/images/images/BeautyAndPersonalCare1.jpg'
    },
    {
      name: "Alritz Fairy Lantern Craft Kit",
      category: 'Arts and Crafts',
      path: '/arts_and_crafts',
      images: '/images/images/ArtsandCrafts.jpg'
    },
    {
      name: "Binder Clips Paper Clips – Sopito 300pcs Colored Office Clips",
      category: 'Office Tools',
      path: '/office_tools',
      images: '/images/images/OfficeTools1.jpg'
    },
    {
      name: "Crazy Skates City Series Foldable Kick Scooter",
      category: 'Sports and Out doors',
      path: '/Sports and Out doors',
      images: '/images/images/SportsandOutdoors1.jpg'
    },
    {
      name: "Holikme 4Pack Drill Brush Power Scrubber Cleaning Brush",
      category: 'Tools and Home Improvement',
      path: '/tools_and_home_improvement',
      images: '/images/images/ToolsandHomeImprovement1.jpg'
    },
    {
      name: "Barbie Dreamhouse (3.75-ft) 3-Story Dollhouse Playset with Pool",
      category: 'TOYS',
      path:'/toys',
      images: '/images/images/TOYS1.jpg'
  },


  ]
  return (
    <>
      <div className="bg-ghost relative overflow-hidden py-50px md:h-556px md:py-77px  ">
        <div className="customcontainer mx-auto">
          <Slider {...silder1} className="slider1">
            {sliderProduct?.map((items, index) => {
              return (
                <div className="slider1_item" key={index}>
                  <div className="md:flex md:items-center">
                    <div className="md:w-1/2 md:pl-5 md:border-l md:border-black">
                      <h1 className="text-darkblack font39 font-extrabold mt-4">
                        {items?.name}
                      </h1>
                      <p>{items?.category}</p>
                      <Link href={items?.path}>
                        <a className="h-9 flex justify-center items-center w-126px bg-orange text-white font11 mt-8">
                          Shop Now
                        </a>

                      </Link>
                    </div>
                    <div className="hidden sm:block sm:w-1/2">
                      <img
                        src={items?.images}
                        className="w-96 h-96 object-contain  ml-auto"
                      />
                    </div>
                  </div>
                </div>

              )
            })
            }
          </Slider>
        </div>
      </div>


      <section className="customcontainer mx-auto mt-20 relative">
        <div className="heading">
          <h1 className="text-darkblack font25 font-semibold bg-white relative z-20 pr-1 max-w-max">
            Arts and Crafts
          </h1>
        </div>
        <div className="mt-40px ">
          <Slider {...productslider} className="product_slider1">
            {FeaturedProducts?.map((ele, index) => {
              return (
                <Card
                  key={index}
                  className="w-284px mx-auto sm:mx-0 sm:w-232px "
                  img={ele.images}
                  name={ele.name}
                  discount={ele.discount}
                  discountprice={ele.discount}
                  price={ele.price}
                  describtion={ele?.Description}
                />
              );
            })}
          </Slider>
        </div>
      </section>


      <section className="customcontainer mx-auto mt-20 relative">
        <div className="heading">
          <h1 className="text-darkblack font25 font-semibold bg-white relative z-20 pr-1 max-w-max">
            Baby Products
          </h1>
        </div>
        <div className="mt-40px ">
          <Slider {...productslider} className="product_slider1">
            {babyProduct?.map((ele, index) => {
              return (
                <Card
                  key={index}
                  className="w-284px mx-auto sm:mx-0 sm:w-232px "
                  img={ele.images}
                  name={ele.name}
                  discount={ele.discount}
                  discountprice={ele.discount}
                  price={ele.price}
                  describtion={ele?.Description}
                />
              );
            })}
          </Slider>
        </div>
      </section>

      <section className="customcontainer mx-auto mt-20 relative">
        <div className="heading">
          <h1 className="text-darkblack font25 font-semibold bg-white relative z-20 pr-1 max-w-max">
            Beauty And Personal Care
          </h1>
        </div>
        <div className="mt-40px ">
          <Slider {...productslider} className="product_slider1">
            {BeautyAndPersonalCare?.map((ele, index) => {
              return (
                <Card
                  key={index}
                  className="w-284px mx-auto sm:mx-0 sm:w-232px "
                  img={ele.images}
                  name={ele.name}
                  discount={ele.discount}
                  discountprice={ele.discount}
                  price={ele.price}
                  describtion={ele?.Description}
                />
              );
            })}
          </Slider>
        </div>
      </section>

      <section className="customcontainer mx-auto mt-20 relative">
        <div className="heading">
          <h1 className="text-darkblack font25 font-semibold bg-white relative z-20 pr-1 max-w-max">
            Office Tools
          </h1>
        </div>
        <div className="mt-40px ">
          <Slider {...productslider} className="product_slider1">
            {OfficeTools?.map((ele, index) => {
              return (
                <Card
                  key={index}
                  className="w-284px mx-auto sm:mx-0 sm:w-232px "
                  img={ele.images}
                  name={ele.name}
                  discount={ele.discount}
                  discountprice={ele.discount}
                  price={ele.price}
                  describtion={ele?.Description}
                />
              );
            })}
          </Slider>
        </div>
      </section>

      <section className="customcontainer mx-auto mt-20 relative">
        <div className="heading">
          <h1 className="text-darkblack font25 font-semibold bg-white relative z-20 pr-1 max-w-max">
            Sports and Outdoors
          </h1>
        </div>
        <div className="mt-40px ">
          <Slider {...productslider} className="product_slider1">
            {SportsandOutdoors?.map((ele, index) => {
              return (
                <Card
                  key={index}
                  className="w-284px mx-auto sm:mx-0 sm:w-232px "
                  img={ele.images}
                  name={ele.name}
                  discount={ele.discount}
                  discountprice={ele.discount}
                  price={ele.price}
                  describtion={ele?.Description}
                />
              );
            })}
          </Slider>
        </div>
      </section>

      <section className="customcontainer mx-auto mt-20 relative">
        <div className="heading">
          <h1 className="text-darkblack font25 font-semibold bg-white relative z-20 pr-1 max-w-max">
            Tools and Home Improvement
          </h1>
        </div>
        <div className="mt-40px ">
          <Slider {...productslider} className="product_slider1">
            {ToolsandHomeImprovement?.map((ele, index) => {
              return (
                <Card
                  key={index}
                  className="w-284px mx-auto sm:mx-0 sm:w-232px "
                  img={ele.images}
                  name={ele.name}
                  discount={ele.discount}
                  discountprice={ele.discount}
                  price={ele.price}
                  describtion={ele?.Description}
                />
              );
            })}
          </Slider>
        </div>
      </section>

      <section className="customcontainer mx-auto mt-20 relative">
        <div className="heading">
          <h1 className="text-darkblack font25 font-semibold bg-white relative z-20 pr-1 max-w-max">
            TOYS
          </h1>
        </div>
        <div className="mt-40px ">
          <Slider {...productslider} className="product_slider1">
            {TOYS?.map((ele, index) => {
              return (
                <Card
                  key={index}
                  className="w-284px mx-auto sm:mx-0 sm:w-232px "
                  img={ele.images}
                  name={ele.name}
                  discount={ele.discount}
                  discountprice={ele.discount}
                  price={ele.price}
                  describtion={ele?.Description}
                />
              );
            })}
          </Slider>
        </div>
      </section>
    </>
  );
}
