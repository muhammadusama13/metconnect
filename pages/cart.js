import React from "react";


const Cart = () => {
  const cartList = [0, 1];
  return (
    <>
      <section className="customcontainer mx-auto mt-14 sm:flex">
        <section className="sm:w-4/6">
          <h4 className="text-sm text-darkblack font-semibold">CART</h4>
     
        </section>
        <section className="sm:w-2/6 sm:pl-30px mt-10 sm:mt-0">
          <h4 className="text-sm text-darkblack font-semibold">Summary</h4>
          <section className="bg-gray-100 px-4 py-7  mt-3">
            <div className="flex justify-between">
              <h6 className="text-darkblack  text-sm font-semibold">
                Cart Subtotal
              </h6>
              <span className="text-darkblack  text-sm font-medium ">
                Rs. 15000
              </span>
            </div>
            <h5 className="text-darkblack  text-sm font-medium mt-5">
              Delivery
            </h5>
            <section className="mt-4 flex justify-between">
              <label className="flex items-center cart__checkbox">
                <input
                  type="radio"
                  className="h-4 w-4 inputRadio__color"
                  name="delivery"
                  value="delivery1"
                />
                <i>
                  <span></span>
                </i>
                <span className="text-darkblack text-sm ml-2.5">
                  Express Delivery
                </span>
              </label>
              <p className="text-sm text-darkblack ">Rs. 300</p>
            </section>
            <section className="mt-4 flex justify-between">
              <label className="flex items-center cart__checkbox">
                <input
                  type="radio"
                  className="h-4 w-4 inputRadio__color"
                  name="delivery"
                  value="delivery1"
                />
                <i>
                  <span></span>
                </i>
                <span className="text-darkblack text-sm ml-2.5">
                  Regular Delivery
                </span>
              </label>
              <p className="text-sm text-darkblack ">Rs. 150</p>
            </section>
            <div className="flex justify-between py-4 border-t mt-5">
              <h6 className="text-darkblack  text-sm font-semibold">
                Cart Total
              </h6>
              <span className="text-darkblack  text-sm font-medium ">
                Rs. 15000
              </span>
            </div>
            <button className="bg-orange h-50px w-full text-lg font-semibold text-white mt-3">
              PROCEED TO CHECKOUT
            </button>
          </section>
          <div className="mt-5  ">
            <h4 className="text-sm text-darkblack font-semibold ">Coupon</h4>
            <div className="lg:flex">
              <input
                type="text"
                placeholder="Enter Code Here"
                className="bg-gray-100 h-50px pl-4 w-full lg:flex-1"
              />
              <button className="bg-orange text-white font-semibold h-50px w-full lg:w-70px ">
                APPLY
              </button>
            </div>
          </div>
        </section>
      </section>
    </>
  );
};

export default Cart;
