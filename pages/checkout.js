import React, { useState } from "react";

const Checkout = () => {
  const [orderComplete, setOrderComplete] = useState(true);
  return (
    <>
      {orderComplete ? (
        <section className="customcontainer mx-auto mt-14 sm:flex">
          <section className="sm:w-1/2">
            <h4 className="text-sm text-darkblack font-semibold">Checkout</h4>
            <form className="bg-gray-100 px-4 sm:px-16 py-10">
              <div className="mb-5">
                <label className="text-sm text-darkblack block">Name</label>
                <input
                  type="text"
                  placeholder="Enter your full name"
                  className="h-50px w-full pl-4 bg-white"
                />
              </div>
              <div className="mb-5">
                <label className="text-sm text-darkblack block">
                  Mobile Number
                </label>
                <input
                  type="number"
                  placeholder="Enter your mobile number"
                  className="h-50px w-full pl-4 bg-white"
                />
              </div>
              <div className="mb-5">
                <label className="text-sm text-darkblack block">
                  Email (Optional)
                </label>
                <input
                  type="email"
                  placeholder="Enter your Email"
                  className="h-50px w-full pl-4 bg-white"
                />
              </div>
              <div className="mb-5">
                <label className="text-sm text-darkblack block">Address</label>
                <input
                  type="text"
                  placeholder="Enter your address"
                  className="h-50px w-full pl-4 bg-white"
                />
              </div>
              <div className="mb-5">
                <textarea
                  className="h-126px w-full p-4 bg-white resize-none "
                  placeholder="Note to Rider (Optional)"
                />
              </div>
              <div>
                <label className="text-sm text-darkblack block font-medium">
                  Payment Methode
                </label>
                <label className="flex items-center cart__checkbox mt-4">
                  <input
                    type="checkbox"
                    className="h-4 w-4 inputRadio__color"
                  />
                  <i>
                    <span></span>
                  </i>
                  <span className="text-darkwhite text-sm ml-2.5">
                    Cash On Delivery
                  </span>
                </label>
              </div>
            </form>
          </section>
          <section className="sm:w-1/2 sm:pl-30px mt-5 sm:mt-0">
            <h4 className="text-sm text-darkblack font-semibold">
              Order Detail
            </h4>
            <section className="bg-gray-100 px-4 sm:px-8 py-10">
              <div className="flex justify-between border-b py-4">
                <p className="text-sm text-darkblack font-medium">Subtotal</p>
                <span className="text-sm text-darkblack font-medium">
                  Rs. 15150
                </span>
              </div>
              <div className="flex justify-between border-b py-6">
                <p className="text-sm text-darkblack font-medium">
                  Coupon Discount
                </p>
                <div>
                  <span className="text-sm text-darkblack font-medium mr-4">
                    Rs. 15150
                  </span>
                  <span className="text-sm text-orange font-medium underline">
                    Remove
                  </span>
                </div>
              </div>
              <div className="flex justify-between border-b py-6">
                <p className="text-sm text-darkblack font-medium">
                  Payment Methode
                </p>
                <span className="text-sm text-darkblack font-medium">
                  Cash on Delivery
                </span>
              </div>
              <div className="flex justify-between border-b py-6">
                <p className="text-sm text-darkblack font-medium">Total</p>
                <span className="text-sm text-darkblack font-medium">
                  15000
                </span>
              </div>
              <button
                className="bg-orange text-white text-lg font-semibold h-50px w-217px mt-16"
                onClick={() => setOrderComplete(false)}
              >
                PLACE ORDER
              </button>
            </section>
          </section>
        </section>
      ) : (
        <>
          <section className="about__banner">
            <div className="w-full h-full banner_heading"></div>
          </section>
          <section className="bg-gray-100 mt-14 w-2/6 mx-auto py-7">
            <h2 className="text-darkblack text-lg font-medium text-center">
              Your order has been placed
            </h2>
            <img
              src="images/check.svg"
              className="h-116px w-116px block mx-auto my-30px"
            />
            <h3 className="text-darkblack text-lg font-medium text-center">
              Thank you for visiting us
            </h3>
            <p className="text-litegray text-lg font-medium text-center">
              Your tracking ID is 1265
            </p>
            <button
              className="w-20 h-50px bg-darkblack mt-30px block mx-auto text-white font-semibold"
              onClick={() => setOrderComplete(true)}
            >
              OK
            </button>
          </section>
        </>
      )}
    </>
  );
};

export default Checkout;
