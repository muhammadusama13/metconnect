import React, { useState } from "react";
import Card from "../components/productcard/card";
import Inputrange from "react-input-range";
import "react-input-range/lib/css/index.css";
import { FeaturedProducts } from "../components/Products/FeaturedProducts";
import { babyProduct } from "../components/Products/BabyProducts";
import { BeautyAndPersonalCare } from "../components/Products/BeautyAndPersonalCare";
import { OfficeTools } from "../components/Products/OfficeTools";
import { SportsandOutdoors } from "../components/Products/SportsandOutdoors";
import { ToolsandHomeImprovement } from "../components/Products/ToolsandHomeImprovement";
import { TOYS } from "../components/Products/TOYS";

const Shop = () => {
  const filter = [
    {
      itemname: "Arts and Crafts",
      totalitem: FeaturedProducts?.length,
    },
    {
      itemname: "Baby Products",
      totalitem: FeaturedProducts?.length,
    },
    {
      itemname: "Beauty And Personal Care",
      totalitem: BeautyAndPersonalCare?.length,
    },
    {
      itemname: "Office Tools",
      totalitem: OfficeTools?.length,
    },
    {
      itemname: "Sports and Outdoors",
      totalitem: SportsandOutdoors?.length,
    },
    {
      itemname: "Tools and Home Improvement",
      totalitem: FeaturedProducts?.length,
    },
    {
      itemname: "TOYS",
      totalitem: FeaturedProducts?.length,
    },
    {
      itemname: "Sports and Outdoors",
      totalitem: FeaturedProducts?.length,
    },
  ];
  const [price, setPrice] = useState({
    min: 1,
    max: 100,
  });
  const recent = [
    {
      itemimg: "images/rect1.png",
      name: "Colored Chair",
      itemprice: "$25.00",
    },
    {
      itemimg: "images/rect2.png",
      name: "Decoration",
      itemprice: "$30.00",
    },
    {
      itemimg: "images/rect3.png",
      name: "Office Chair",
      itemprice: "$40.00",
    },
  ];
  const shopcard = [...FeaturedProducts, ...babyProduct,
  ...BeautyAndPersonalCare,
  ...OfficeTools, ...SportsandOutdoors, ...ToolsandHomeImprovement, ...TOYS]
  const [filt, setfilt] = useState(false);

  return (
    <>
      <section className="bg_banner">
        <div className="w-full h-full banner_heading">
          <h1 className="text-darkblack">Products Store</h1>
        </div>
      </section>
      <section className="customcontainer mx-auto mt-14 lg:flex">
        <div
          className={`${filt ? "filter_show" : "filter_hide"
            } fixed z-50 top-0 w-full h-full bg-white overflow-y-scroll lg:overflow-auto left-0 px-4 md: lg:static lg:w-1/4 transition-all duration-200`}
        >
          <button
            className="my-4 ml-auto block lg:hidden"
            onClick={() => setfilt(!filt)}
          >
            <img src="images/cross.svg" />
          </button>
          <div>
            <h1 className="text-darkblack text-lg font-bold pl-30px filter_title">
              CATEGORY
            </h1>
            <div className="mt-9">
              {filter.map((ele, index) => {
                return (
                  <div
                    className="border-b flex justify-between items-center py-2"
                    key={index}
                  >
                    <p className="text-sm text-darkblack">{ele.itemname}</p>
                    <p className="text-sm text-darkwhite">{ele.totalitem}</p>
                  </div>
                );
              })}
            </div>
          </div>
          <div className="mt-82px">
            <h1 className="text-darkblack text-lg font-bold pl-30px filter_title">
              PRICE
            </h1>
            <div className="mt-8">
              <Inputrange
                maxValue={100}
                minValue={0}
                step={1}
                value={price}
                onChange={setPrice}
              />
            </div>
            <div className="flex justify-between mt-5">
              <p className="text-sm text-darkblack">{`$${price.min}.00`}</p>
              <p className="text-sm text-darkblack">{`$${price.max}.00`}</p>
            </div>
          </div>
        </div>
        <div className="w-full pl-0 lg:w-3/4 lg:pl-7">
          <div className="grid grid-cols-1 sm:grid-cols-3 lg:grid-cols-3 lg:gap-7 mt-4">
            {shopcard?.map((ele, index) => {
              return (
                <Card
                  key={index}
                  className="w-284px mx-auto sm:mx-0 sm:w-232px "
                  img={ele.images}
                  name={ele.name}
                  discount={ele.discount}
                  discountprice={ele.discount}
                  price={ele.price}
                  describtion={ele?.Description}
                />
              );
            })}
          </div>
        </div>
      </section>
    </>
  );
};

export default Shop;
